/* WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function*/




// Creating an object constructor
function Pokemon (name, level) {

	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		target.health -= this.attack;
        if (target.health <= 5) {
            target.faint();
        };

};
	this.faint = function( ){
		console.log(this.name + " fainted.")
	};

};

let ratata = new Pokemon ("Ratata", 20);
let diglet = new Pokemon ("Diglet", 20);

console.log(ratata)
console.log(diglet)


ratata.tackle(diglet);
ratata.tackle(diglet);
ratata.tackle(diglet);


